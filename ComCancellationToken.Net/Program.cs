﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ComCancellationTokenLib;

namespace ComCancellationToken.Net
{
    class Program
    {
        static void Main(string[] args)
        {
            var sampleCOMClass = new SampleCOMObjectClass();
            var cancelToken = new CancelCOMObjectClass();
            try
            {
                Task.Factory.StartNew(() =>
                {
                    Thread.Sleep(TimeSpan.FromSeconds(10));
                    cancelToken.Cancel(); // this method never makes it to COM
                    Console.WriteLine("Cancelled!");
                });
                sampleCOMClass.LongProcess(cancelToken);
            }
            finally
            {
                Marshal.ReleaseComObject(sampleCOMClass);
                Marshal.ReleaseComObject(cancelToken);
            }
        }
    }
}
