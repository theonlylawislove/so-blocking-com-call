// CancelCOMObject.cpp : Implementation of CCancelCOMObject

#include "stdafx.h"
#include "CancelCOMObject.h"


// CCancelCOMObject



STDMETHODIMP CCancelCOMObject::Cancel(void)
{
	_isCancelled = VARIANT_TRUE;
    return S_OK;
}


STDMETHODIMP CCancelCOMObject::get_IsCancelled(VARIANT_BOOL* pVal)
{
	*pVal = _isCancelled;
    return S_OK;
}