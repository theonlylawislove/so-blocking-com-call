// CancelCOMObject.h : Declaration of the CCancelCOMObject

#pragma once
#include "resource.h"       // main symbols



#include "ComCancellationToken_i.h"


#ifdef _WIN32_WCE
#error "Neutral-threaded COM objects are not supported on Windows CE."
#endif

using namespace ATL;


// CCancelCOMObject

class ATL_NO_VTABLE CCancelCOMObject :
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<CCancelCOMObject, &CLSID_CancelCOMObject>,
	public IDispatchImpl<ICancelCOMObject, &IID_ICancelCOMObject, &LIBID_ComCancellationTokenLib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	CCancelCOMObject()
	{
		_isCancelled = VARIANT_FALSE;
	}

DECLARE_REGISTRY_RESOURCEID(IDR_CANCELCOMOBJECT)


BEGIN_COM_MAP(CCancelCOMObject)
	COM_INTERFACE_ENTRY(ICancelCOMObject)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()



	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:
	STDMETHOD(Cancel)(void);
	STDMETHOD(get_IsCancelled)(VARIANT_BOOL* pVal);
private:
	VARIANT_BOOL _isCancelled;
};

OBJECT_ENTRY_AUTO(__uuidof(CancelCOMObject), CCancelCOMObject)
