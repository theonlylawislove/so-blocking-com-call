// ComCancellationToken.cpp : Implementation of WinMain


#include "stdafx.h"
#include "resource.h"
#include "ComCancellationToken_i.h"
#include "xdlldata.h"


using namespace ATL;


class CComCancellationTokenModule : public ATL::CAtlExeModuleT< CComCancellationTokenModule >
{
public :
	DECLARE_LIBID(LIBID_ComCancellationTokenLib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_COMCANCELLATIONTOKEN, "{C72220FE-F9C2-4607-BBD6-92839ACCDCA9}")
	};

CComCancellationTokenModule _AtlModule;



//
extern "C" int WINAPI _tWinMain(HINSTANCE /*hInstance*/, HINSTANCE /*hPrevInstance*/, 
								LPTSTR /*lpCmdLine*/, int nShowCmd)
{
	return _AtlModule.WinMain(nShowCmd);
}

