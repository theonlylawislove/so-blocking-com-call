// SampleCOMObject.cpp : Implementation of CSampleCOMObject

#include "stdafx.h"
#include "SampleCOMObject.h"


// CSampleCOMObject



STDMETHODIMP CSampleCOMObject::LongProcess(ICancelCOMObject* cancel)
{
	VARIANT_BOOL isCancelled = VARIANT_FALSE;
	while(isCancelled == VARIANT_FALSE)
	{
		Sleep(1000);
		cancel->get_IsCancelled(&isCancelled);
	}
	return S_OK;
}
