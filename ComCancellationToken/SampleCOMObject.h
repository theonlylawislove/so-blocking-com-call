// SampleCOMObject.h : Declaration of the CSampleCOMObject

#pragma once
#include "resource.h"       // main symbols



#include "ComCancellationToken_i.h"


#ifdef _WIN32_WCE
#error "Neutral-threaded COM objects are not supported on Windows CE."
#endif

using namespace ATL;


// CSampleCOMObject

class ATL_NO_VTABLE CSampleCOMObject :
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<CSampleCOMObject, &CLSID_SampleCOMObject>,
	public IDispatchImpl<ISampleCOMObject, &IID_ISampleCOMObject, &LIBID_ComCancellationTokenLib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	CSampleCOMObject()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_SAMPLECOMOBJECT)


BEGIN_COM_MAP(CSampleCOMObject)
	COM_INTERFACE_ENTRY(ISampleCOMObject)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()



	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:



	STDMETHOD(LongProcess)(ICancelCOMObject* cancel);
};

OBJECT_ENTRY_AUTO(__uuidof(SampleCOMObject), CSampleCOMObject)
